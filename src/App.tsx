import * as React from 'react';


import HashRouteController from "./HashRouteController";
import AboutMePage from "./Pages/AboutMePage";
import GalleryPage from "./Pages/GalleryPage/GalleryPage";
import InspirationalPeoplePage from "./Pages/InspirationalPeoplePage";
import WelcomePage from "./Pages/WelcomePage";


interface IAppState {
    selectedPage: string,
    enableSmallScreen: boolean,
    unparsedRoute: string[],
}


class App extends React.Component<object, IAppState> {

    public static SmallScreenSize = 800;

    public static Routes = {
        "#AboutMe": AboutMePage,
        "#Gallery": GalleryPage,
        "#InspirationalPeople": InspirationalPeoplePage,
        "#Welcome": WelcomePage,
    }

    public static Pages = [
        WelcomePage,
        AboutMePage,
        GalleryPage,
        InspirationalPeoplePage
    ];

    constructor(args: any) {
        super(args);

        this.onWindowResize = this.onWindowResize.bind(this);
        this.renderSmallScreen = this.renderSmallScreen.bind(this);
        this.renderNormalScreen = this.renderNormalScreen.bind(this);
        this.renderLinks = this.renderLinks.bind(this);
        this.renderSelectedPage = this.renderSelectedPage.bind(this);
        this.splitRoute = this.splitRoute.bind(this);

        const route = this.splitRoute();
        this.state = {
            enableSmallScreen: window.innerWidth < App.SmallScreenSize,
            selectedPage: route[0],
            unparsedRoute: route.slice(1)
        };

    }

    get selectedPageElement(): JSX.Element {
        return App.Routes[this.state.selectedPage];
    }

    public componentDidMount() {
        HashRouteController.onhashchange = () => {
            const route = this.splitRoute();
            this.setState({
                selectedPage: route[0],
                unparsedRoute: route.slice(1)
            });
        }

        window.onresize = this.onWindowResize;
    }

    public componentWillUnmount() {
        HashRouteController.onhashchange = null;
        // window.onresize = null; does not compile... wtf..
        // tslint:disable-next-line:no-empty
        window.onresize = () => {};
    }

    public onWindowResize() {
        const enableSmallScreen = window.innerWidth < App.SmallScreenSize;

        if (this.state.enableSmallScreen !== enableSmallScreen) {
            this.setState({
                enableSmallScreen
            });
        }

    }

    public render() {
        if (this.state.enableSmallScreen) {
            return this.renderSmallScreen();
        } else {
            return this.renderNormalScreen();
        }
    }

    public renderSmallScreen() {
        return <div
                style={{
                    display: "flex",
                }}
        > 
            <div
                style={{
                    flexBasis: "50px",
                    flexShrink: 2
                }}
            />
            <div
                style={{
                    display: "flex",
                    flexBasis: "700px",
                    flexDirection: "column",
                    flexShrink: 1,
                }}
            >
                {this.renderLinks()}
        
                <div
                    style={{
                        flexBasis: "20px",
                        flexShrink: 2
                    }}
                />
                { this.renderSelectedPage() }
                
            </div>
            <div
                style={{
                    flexBasis: "50px",
                    flexShrink: 2
                }}
            />
        </div>
    }

    public renderNormalScreen() {
        return <div 
            style={{
                display: "flex",
                flexDirection: "column"
            }}
        >
            <div
                style={{
                    flexBasis: "100px",
                    flexShrink: 2
                }}
            />
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    flexShrink: 1,
                }}
            >
                <div
                    style={{
                        display: "flex",
                        flexShrink: 2,
                        width: "70%",
                        
                    }}
                > 
                    <div
                        style={{
                            flexBasis: "200px",
                            flexGrow: 1
                        }}
                    />
                    <div
                        style={{
                            flexBasis: "500px",
                            flexGrow: 2
                        }}
                    >
                        { this.renderSelectedPage() }
                    </div>
                    <div
                        style={{
                            flexBasis: "200px",
                            flexGrow: 1
                        }}
                    />
                </div>
                <div
                    style={{
                        flexShrink: 1,
                        width: "30%",
                    }}
                >
                    <div style={{height: "50px"}}/>
                    {this.renderLinks()}
                </div>
            </div>
        </div>
        
    }

    private renderSelectedPage(): JSX.Element {
        const E = App.Routes[this.state.selectedPage];
        return <E
            unparsedRoute = {this.state.unparsedRoute}
        />
    }

    private renderLinks(): JSX.Element[] {
        const result = [];
        let key = 0;        
        for (const page of App.Pages) {
            if (page.renderLink == null) {
                continue;
            }
            
            const link = page.renderLink(key++, (route: string) => {
                window.location.hash = route
            });

            result.push(link);
        }
        
        return result;
        
    }
    

    private splitRoute(): string[] {
        const result = window.location.hash.split("/");
        if (result == null || result.length === 0 || App.Routes[result[0]] == null) {
            return [ "#Welcome" ];
        }
        
        return result;
    }

}

export default App;
