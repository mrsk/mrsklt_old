
import * as React from 'react';
import PageContent from "src/Page/PageContent";
import PageLink from "src/Page/PageLink";
import PageTitle from "src/Page/PageTitle";


class WelcomePage extends React.Component {
    
    public static renderLink(key: number, onSelect: any): JSX.Element {
        
        return <PageLink 
            key={key}
            onClickArgs="#Welcome"
            onClick={onSelect}
        >
            Welcome!
        </PageLink>
        
        
    }

    constructor(args: any) {
        super(args);

        this.renderTitle = this.renderTitle.bind(this);
        this.renderContent = this.renderContent.bind(this);

    }

    public render() {
        return <div>
            {this.renderTitle()}
            {this.renderContent()}
        </div>
    }


    public renderTitle() {
        return <PageTitle>
            Welcome
        </PageTitle>
    }


    public renderContent() {
        return <PageContent>

            Welcome to this over-engineered, all static, reactjs website.
            It is hosted somewhere sometimes, via Inter Planetary File System (IPFS) network.
            Some larger media files are hosted on <a href="https://ovh.mrsk.lt">https://ovh.mrsk.lt</a>.
            Publicly trusted TLS certificate is provided by Cloudflare services.
            You can dirrectly visit this website on <a href="https://gateway.ipfs.io/ipns/mrsk.lt">public IPFS gateway</a>. 
            Source code is available at <a href="https://gitlab.com/mrsk/mrsklt">gitlab</a>.
            
            <br/>
            <br/>

            Any views, opinions or ideas expressed here are my own (they could be out of date though) 
            and do not in any way represent the views of any other entity.

            <br/>
            <br/>

            Enjoy:)

        </PageContent>
    }


}

export default WelcomePage;
