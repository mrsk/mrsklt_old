
import * as React from "react";
import PageContent from "src/Page/PageContent";
import PageTitle from "src/Page/PageTitle";
import Lightbox from 'react-images';
import urlJoin from 'proper-url-join';


interface INginxFGalleryPageProps {
    fGalleryUrl: string,
    title: string,
    unparsedRoute: string[],
    setRoute: (f: (baseRoute: string) => string) => void
}

interface INginxFGalleryPageState {
    images: any[],
    listLoaded: boolean,
    previewIsOpen: boolean,
    previewIndex: number
}

class NginxFGalleryPage extends React.Component<INginxFGalleryPageProps, INginxFGalleryPageState> {
    
    constructor(args: any) {
        super(args);

        this.renderTitle = this.renderTitle.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.renderGallery = this.renderGallery.bind(this);
        this.renderPreview = this.renderPreview.bind(this);

        this.findUrl = this.findUrl.bind(this);
        this.state = {
            images: [],
            previewIsOpen: false,
            listLoaded: false,
            previewIndex: 0
        };
    }

    public componentDidMount() {
        const dirUrl = urlJoin(this.props.fGalleryUrl, "imgs", { leadingSlash: false });
        fetch(dirUrl)
            .then(response => response.text()) // FIXME parse xml maybe
            .then(responseText => {
                const lines = responseText.match(/[^\r\n]+/g);
                const images = [];
                if (lines == null) {
                    return;
                }
                for (const line of lines) {
                    const [original, thumbnail] = this.findUrl(line);
                    if (original !== "" && thumbnail !== "") {
                        images.push({
                            original,
                            thumbnail
                        });
                    }
                }
                
                let previewIndex = null;
                if (this.props.unparsedRoute != null
                    && this.props.unparsedRoute.length > 0
                ) {
                    const parsed = Number.parseInt(this.props.unparsedRoute[0]);
                    if (parsed < images.length && parsed >= 0) {
                        previewIndex = parsed;
                    }
                }

                this.setState({
                    images,
                    listLoaded: true,
                    previewIsOpen: previewIndex != null,
                    previewIndex: previewIndex || 0
                });

            });
            // FIXME catch maybe?
    }

    public render() {
        if (!this.state.listLoaded) {
            return "loading..."; // FIXME
        }
        return <div>
            {this.renderTitle()}
            {this.renderContent()}
        </div>
    }


    public renderTitle() {
        return <PageTitle>
            {this.props.title}
        </PageTitle>
    }


    public renderContent() {
        return <PageContent>

            {this.renderGallery()}
            {this.renderPreview()}
            

        </PageContent>
    }

    private renderGallery(): JSX.Element {



        return <div
            style={{
                display: "flex",
                flexWrap: "wrap",
                flexDirection: "row"
            }}
        >
            {this.state.images.map((img, i) => {
                return <img 
                    key={i}
                    src={img.thumbnail}
                    // tslint:disable-next-line:jsx-no-lambda
                    onClick={() => {
                        this.setState({
                            previewIndex: i,
                            previewIsOpen: true
                        });
                        this.props.setRoute(baseRoute => urlJoin(baseRoute, i, { leadingSlash: false }));
                    }}
                    style={{
                        cursor: "pointer"
                    }}
                />
            })}
        </div>

    }

    private renderPreview(): JSX.Element {
        const images = [];
        for (const i of this.state.images) {
            images.push({
                src: i.original
            });
        }

        return <Lightbox 
            images={images}
            isOpen={this.state.previewIsOpen}
            // tslint:disable-next-line:jsx-no-lambda
            onClose={() => {
                this.setState({
                    previewIsOpen: !this.state.previewIsOpen
                });
                this.props.setRoute(baseRoute => baseRoute);
            }}
            currentImage={this.state.previewIndex}
            // tslint:disable-next-line:jsx-no-lambda
            onClickPrev={() => {
                let previewIndex = (this.state.previewIndex - 1) % this.state.images.length;
                if (previewIndex < 0) {
                    previewIndex += this.state.images.length;
                }
                this.setState({
                    previewIndex
                });
                this.props.setRoute(baseRoute => urlJoin(baseRoute, previewIndex, { leadingSlash: false }));
            }}
            // tslint:disable-next-line:jsx-no-lambda
            onClickNext={() => {
                let previewIndex = (this.state.previewIndex + 1) % this.state.images.length;
                if (previewIndex < 0) {
                    previewIndex += this.state.images.length;
                }
                this.setState({
                    previewIndex
                });
                this.props.setRoute(baseRoute => urlJoin(baseRoute, previewIndex, { leadingSlash: false }));
            }}
            preloadNextImage={true}
        />
    }

    // nginx autoindex format:
    // <a href="IMG_20180705_114604.jpg">IMG_20180705_114604.jpg</a>                            15-Jul-2018 09:39              349528
    private findUrl(text: string): [string, string] {
        if (!text.startsWith("<a href=\"")) {
            return ["", ""];
        }

        const urlEndIndex = text.indexOf("\">");
        const imgName = text.slice(9, urlEndIndex);
        const original = urlJoin(this.props.fGalleryUrl, "imgs", imgName, { leadingSlash: false });
        const thumbnail = urlJoin(this.props.fGalleryUrl, "thumbs", imgName, { leadingSlash: false });

        return [original, thumbnail];
    }

}

export default NginxFGalleryPage;
