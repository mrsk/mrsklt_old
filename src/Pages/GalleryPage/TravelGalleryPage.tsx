
import * as React from "react";
import NginxFGalleryPage from "./NginxFGalleryPage";
import HashRouteController from "src/HashRouteController";

interface ITravelGalleryPageProps {
    unparsedRoute: string[],
}

class TravelGalleryPage extends React.Component<ITravelGalleryPageProps> {
    

    public render() {
        
        return <NginxFGalleryPage
            fGalleryUrl="https://ovh.mrsk.lt/pics/austria/"
            title="Travel pictures"
            unparsedRoute={this.props.unparsedRoute}
            // tslint:disable-next-line:jsx-no-lambda
            setRoute={(getRoute) => {
                const baseRoute = "#Gallery/Travel";
                const route = getRoute(baseRoute);
                HashRouteController.setHashRoute(route);
            }}
        />

    }
    

}

export default TravelGalleryPage;
