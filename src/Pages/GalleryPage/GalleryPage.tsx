
import * as React from 'react';
import PageContent from "src/Page/PageContent";
import PageLink from "src/Page/PageLink";
import PageTitle from "src/Page/PageTitle";
import TravelGalleryPage from "./TravelGalleryPage";
import FilmPhotoGalleryPage from  "./FilmPhotoGalleryPage";

interface IGalleryPageProps {
    unparsedRoute: string[],
}

interface IGalleryPageState {
    selectedGallery: string
}

class GalleryPage extends React.Component<IGalleryPageProps, IGalleryPageState> {
    
    public static Routes = {
        "Travel": TravelGalleryPage,
        "FilmPhoto": FilmPhotoGalleryPage
    }

    public static renderLink(key: number, onSelect: any): JSX.Element {
        
        return <PageLink 
            key={key}
            onClickArgs="#Gallery"
            onClick={onSelect}
        >
            Gallery
        </PageLink>
        
        
    }

    constructor(args: any) {
        super(args);

        this.renderTitle = this.renderTitle.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.getSelectedGallery = this.getSelectedGallery.bind(this);

    }



    public render() {
        const selectedGallery = this.getSelectedGallery();
        if (selectedGallery !== "") {

            const E = GalleryPage.Routes[selectedGallery];

            return <div>
                <div><a className="PageLink" href="#Gallery">{"⬑ back to gallery list"}</a></div>
                
    
                <E unparsedRoute={this.props.unparsedRoute.slice(1)}/>
            </div>

        } else {
            return <div>
                {this.renderTitle()}
                {this.renderContent()}
            </div>
        }


    }


    public renderTitle() {
        return <PageTitle>
            Gallery
        </PageTitle>
    }


    public renderContent() {
        // FIXME make <a><span></span></a>
        return <PageContent>
            
            <div><a className="PageLink" href="#Gallery/Travel">Travel pictures</a></div>
            <div><a className="PageLink" href="#Gallery/FilmPhoto">Film photography</a></div>
        
        </PageContent>
    }

    private getSelectedGallery(): string {
        if (this.props.unparsedRoute == null
            || this.props.unparsedRoute.length === 0
            || GalleryPage.Routes[this.props.unparsedRoute[0]] == null
        ) {
            return "";
        }

        return this.props.unparsedRoute[0];
    }

    // private getUnparsedRoute(): string[] {
    //     if (this.props.unparsedRoute == null
    //         || this.props.unparsedRoute.length === 0
    //         || GalleryPage.Routes[this.props.unparsedRoute[0]] == null
    //     ) {
    //         return [];
    //     }
    //     return this.props.unparsedRoute.slice(1);
    // }


}

export default GalleryPage;
