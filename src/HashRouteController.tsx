

class HashRouteController {

    public static onhashchange: any = null;

    public static disableHandler = false;

    public static setHashRoute(hashRoute: string) {
        HashRouteController.disableHandler = true;
        window.location.hash = hashRoute;
        HashRouteController.disableHandler = false;
    }

    public static init() {
        window.onhashchange = () => {
            
            if (HashRouteController.disableHandler) {
                return;
            }

            if (HashRouteController.onhashchange == null) {
                return;
            }

            HashRouteController.onhashchange!();
        }
    }


}

export default HashRouteController;
