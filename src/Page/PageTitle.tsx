
import * as React from 'react';


class PageTitle extends React.Component {


    public render() {
        return <div
            style={{
                color: "rgb(180, 194, 75)",
                fontFamily: "Tahoma",
                fontSize: "26px",
                marginBottom: "26px"
            }}
        >
            {this.props.children}
        </div>
    }

}

export default PageTitle;
