
import * as React from 'react';


class PageContent extends React.Component {


    public render() {
        return <div
            style={{
                color: "rgb(85, 85, 85)",
                fontFamily: "Georgia",
                fontSize: "16px",
                lineHeight: "24px"
            }}
        >
            {this.props.children}
        </div>
    }

}

export default PageContent;
